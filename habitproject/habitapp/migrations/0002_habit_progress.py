# Generated by Django 3.1.2 on 2021-01-19 13:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('habitapp', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='habit',
            name='progress',
            field=models.IntegerField(default=0),
        ),
    ]
